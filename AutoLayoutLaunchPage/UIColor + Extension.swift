//
//  UIColor + Extension.swift
//  AutoLayoutLaunchPage
//
//  Created by Hy Chhayrith on 1/28/19.
//  Copyright © 2019 Chhayrithhy. All rights reserved.
//

import UIKit

extension UIColor {
    static var mainPink = UIColor(red: 232/255, green: 68/255, blue: 133/255, alpha: 1)
}
