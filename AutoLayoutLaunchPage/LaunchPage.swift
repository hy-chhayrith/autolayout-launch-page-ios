//
//  LaunchPage.swift
//  AutoLayoutLaunchPage
//
//  Created by Hy Chhayrith on 1/29/19.
//  Copyright © 2019 Chhayrithhy. All rights reserved.
//

import UIKit

extension UIColor {
    static var pageControlIndicatorColor = UIColor(red:0.61, green:0.61, blue:0.61, alpha:1.0)
}

class LaunchPage: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let pages = [
        Page(imageName: "Pear_diamond", headerText: "Pear Diamond", bodyText: "Reminiscent of a tear drop, a pear shaped diamond blends the best of the round and marquise diamond shapes."),
        Page(imageName: "Rose_Diamond", headerText: "Rose Diamond", bodyText: "The Rose Diamond was one of the large rare gems produced by Premier Mine, of De Beers in South Africa."),
        Page(imageName: "steinmetz_pink_diamond", headerText: "Steinmetz Pink Diamond", bodyText: "The Pink Star, formerly known as the Steinmetz Pink, is a diamond weighing 59.60 carat, rated in color as Fancy Vivid Pink by the Gemological Institute of America."),
        Page(imageName: "koh_i_noor_diamond", headerText: "Koh I Noor Diamond", bodyText: "The diamond came from India’s alluvial mines thousands of years ago, sifted from the sand. "),
        Page(imageName: "allnatt_diamond", headerText: "Allnat Diamond ", bodyText: "The Allnatt Diamond is a diamond measuring 101.29 carats with a cushion cut, rated in color as Fancy Vivid Yellow by the Gemological Institute of America."),
    ]
    
    lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.currentPage = 0
        pageControl.numberOfPages = pages.count
        pageControl.currentPageIndicatorTintColor = .black
        pageControl.pageIndicatorTintColor = .pageControlIndicatorColor
        
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        return pageControl
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        
        pageControl.currentPage = Int(x / view.frame.width)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: scrollView.frame.height / 2)
        if let indexPath = collectionView.indexPathForItem(at: center) {
            self.pageControl.currentPage = indexPath.row
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { (_) in
            self.collectionViewLayout.invalidateLayout()
            if self.pageControl.currentPage == 0 {
                self.collectionView.contentOffset = .zero
            }else {
                let indexPath = IndexPath(row: self.pageControl.currentPage, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
        }) { (_) in
            
        }
    }
    
    
}

/// Mark: life cycle
extension LaunchPage {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(pageControl)
        self.setupPageControl()
        
        self.setupCollectionView()
    }
    
    private func setupPageControl() {
        self.pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.pageControl.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
        self.pageControl.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        self.pageControl.heightAnchor.constraint(equalToConstant: 36).isActive = true
    }
        
    private func setupCollectionView() {
        collectionView.backgroundColor = .black
        collectionView.register(LaunchPageCell.self, forCellWithReuseIdentifier: "cellId")
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
    }
}

/// Mark: conforming protocol
extension LaunchPage {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let launchPageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! LaunchPageCell
        launchPageCell.page = pages[indexPath.row]
        return launchPageCell
    }
    
}
