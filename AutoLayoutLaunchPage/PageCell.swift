//
//  PageCell.swift
//  AutoLayoutLaunchPage
//
//  Created by Hy Chhayrith on 1/28/19.
//  Copyright © 2019 Chhayrithhy. All rights reserved.
//

import UIKit

class PageCell: UICollectionViewCell {
    var page: Page? {
        didSet{
            guard let unwrappedPage = page else { return }
            self.iconImage.image = UIImage(named: unwrappedPage.imageName)
            let attributedText = NSMutableAttributedString(string: unwrappedPage.headerText, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)])
            
            attributedText.append(NSAttributedString(string: "\n\n \(unwrappedPage.bodyText)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor.gray]))
            
            self.text.attributedText = attributedText
            self.text.textAlignment = .center
        }
    }
    
    lazy var iconImage: UIImageView = {
       let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = UIImage(named: "bear_first")
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var iconImageContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var text: UITextView = {
       let text = UITextView()
        let attributeText = NSMutableAttributedString(string: "Join us today in our fun and games!", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)])
        
        attributeText.append(NSAttributedString(string: "Join us today in our fun and games!", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor : UIColor.gray]))
        
        text.attributedText = attributeText
        text.textAlignment = .center
        text.isEditable = false
        text.isScrollEnabled = false
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

/// Mark: setup layout
extension PageCell {
    private func setupLayout() {
        self.addSubview(iconImageContainer)
        
        iconImageContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        iconImageContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        iconImageContainer.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        iconImageContainer.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.5).isActive = true
        
        iconImageContainer.addSubview(iconImage)
        iconImage.centerXAnchor.constraint(equalTo: iconImageContainer.centerXAnchor).isActive = true
        iconImage.centerYAnchor.constraint(equalTo: iconImageContainer.centerYAnchor).isActive = true
        iconImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3).isActive = true
        
        self.addSubview(self.text)
        text.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 24).isActive = true
        text.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -24).isActive = true
        text.topAnchor.constraint(equalTo: iconImageContainer.bottomAnchor, constant: 16).isActive = true
        text.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
}
