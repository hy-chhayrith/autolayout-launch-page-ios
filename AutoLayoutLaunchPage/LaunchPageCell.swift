//
//  LaunchPageCell.swift
//  AutoLayoutLaunchPage
//
//  Created by Hy Chhayrith on 1/29/19.
//  Copyright © 2019 Chhayrithhy. All rights reserved.
//

import UIKit

class LaunchPageCell: UICollectionViewCell {
    let gradient: CAGradientLayer = CAGradientLayer()
    
    var page: Page? {
        didSet {
            guard let unwrappedPage = page else { return }
            let attributedText = NSMutableAttributedString(attributedString: NSAttributedString(string: unwrappedPage.headerText, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 36)]))
            
            attributedText.append(NSAttributedString(string: "\n\n\(unwrappedPage.bodyText)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16)]))
            
            diamondDescription.attributedText = attributedText
            diamondDescription.textAlignment = .center
            diamondDescription.textColor = .white
        
            diamondImage.image = UIImage(named: unwrappedPage.imageName)
        }
    }

    private let imageContainer: UIView = {
       let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    private let diamondImage: UIImageView = {
       let image = UIImageView()
        image.image = UIImage(named: "Pear_diamond")
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    private let diamondDescription: UITextView = {
        let attributedText = NSMutableAttributedString(attributedString: NSAttributedString(string: "Pear Diamond", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 36)]))
        
        attributedText.append(NSAttributedString(string: "\n\nReminiscent of a tear drop, a pear shaped diamond blends the best of the round and marquise diamond shapes.", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16)]))
        
        let text = UITextView()
        text.attributedText = attributedText
        text.textColor = .white
        text.textAlignment = .center
        text.backgroundColor = nil
        text.isScrollEnabled = false
        text.isEditable = false
        text.translatesAutoresizingMaskIntoConstraints = false
        
        return text
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupCellView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCellView() {
        self.setupGradientColor()
        self.layer.addSublayer(self.gradient)
        
        self.addSubview(imageContainer)
        self.addSubview(diamondDescription)
        
        imageContainer.addSubview(diamondImage)
        self.imageContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.imageContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.imageContainer.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor).isActive = true
        self.imageContainer.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4).isActive = true
        
        self.diamondImage.centerXAnchor.constraint(equalTo: self.imageContainer.centerXAnchor).isActive = true
        self.diamondImage.centerYAnchor.constraint(equalTo: self.imageContainer.centerYAnchor).isActive = true
        self.diamondImage.widthAnchor.constraint(equalToConstant: 150).isActive = true
        self.diamondImage.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.diamondDescription.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        self.diamondDescription.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        self.diamondDescription.topAnchor.constraint(equalTo: self.imageContainer.bottomAnchor).isActive = true
        self.diamondDescription.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    private func setupGradientColor() {
        self.gradient.colors = [UIColor.black.cgColor, UIColor.white.cgColor]
        self.gradient.locations = [0.50, 0.85]
        self.gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        self.gradient.endPoint = CGPoint(x: 0.0, y: 0.95)
        self.gradient.frame = self.bounds
    }

}
