//
//  Page.swift
//  AutoLayoutLaunchPage
//
//  Created by Hy Chhayrith on 1/29/19.
//  Copyright © 2019 Chhayrithhy. All rights reserved.
//

import Foundation

struct Page {
    let imageName: String
    let headerText: String
    let bodyText: String
}
