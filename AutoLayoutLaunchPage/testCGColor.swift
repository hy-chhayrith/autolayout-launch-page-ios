//
//  testCGColor.swift
//  AutoLayoutLaunchPage
//
//  Created by Hy Chhayrith on 1/29/19.
//  Copyright © 2019 Chhayrithhy. All rights reserved.
//

import UIKit

class GradientColor: UIViewController {
    
    let gradient: CAGradientLayer = CAGradientLayer()

    override func viewDidLoad() {
        self.gradient.colors = [UIColor.yellow.cgColor , UIColor.white.cgColor]
        self.gradient.locations = [0.0, 1.0]
        self.gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        self.gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.gradient.frame = self.view.bounds

        self.view.layer.addSublayer(self.gradient)
        //view.backgroundColor = .white
        
    }
}
